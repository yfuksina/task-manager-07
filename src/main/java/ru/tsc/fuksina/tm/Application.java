package ru.tsc.fuksina.tm;

import ru.tsc.fuksina.tm.constant.ArgumentConst;
import ru.tsc.fuksina.tm.constant.TerminalConst;
import ru.tsc.fuksina.tm.model.Command;
import ru.tsc.fuksina.tm.util.ByteUtil;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        if (processArgument(args)) System.exit(0);
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.INFO:
                showSystemInfo();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                showErrorCommand(command);
                break;
        }
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument(arg);
                break;
        }
    }

    public static boolean processArgument(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void showWelcome() {
        System.out.println("** WELCOME TO TASK-MANAGER **");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Author: Yana Fuksina");
        System.out.println("Email: yfuksina@t1-consulting.ru");
    }

    public static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = ByteUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryValue = ByteUtil.formatBytes(maxMemory);
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = isMemoryLimit ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = ByteUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVN: " + totalMemoryFormat);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = ByteUtil.formatBytes(usedMemory);
        System.out.println("Used memory in JVN: " + usedMemoryFormat);
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.ABOUT);
        System.out.println(Command.VERSION);
        System.out.println(Command.INFO);
        System.out.println(Command.HELP);
        System.out.println(Command.EXIT);
    }

    public static void showErrorCommand(String arg) {
        System.err.printf("Error! Unknown command `%s`...\n", arg);
    }

    public static void showErrorArgument(String arg) {
        System.err.printf("Error! Unknown argument `%s`...\n", arg);
    }

    public static void close() {
        System.exit(0);
    }

}

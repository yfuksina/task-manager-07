package ru.tsc.fuksina.tm.model;

import ru.tsc.fuksina.tm.constant.ArgumentConst;
import ru.tsc.fuksina.tm.constant.TerminalConst;

public class Command {

    public static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Display developer info"
    );

    public static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Display application version"
    );

    public static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Display list of terminal commands"
    );

    public static Command INFO = new Command (
            TerminalConst.INFO, ArgumentConst.INFO, "Show system info"
    );

    public static Command EXIT = new Command(
            TerminalConst.EXIT, null, "Close application"
    );

    private String name = "";

    private String description = "";

    private String argument = "";

    public Command() {
    }

    public Command(String name) {
        this.name = name;
    }

    public Command(String name, String argument) {
        this.name = name;
        this.argument = argument;
    }

    public Command(final String name, final String argument, final String description) {
        this.name = name;
        this.description = description;
        this.argument = argument;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }
}
